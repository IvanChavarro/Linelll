package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "rol")
public class Rol {

	@Id
	private Integer idRol;
	
	@NotNull(message = "El nombre es obligatorio")
	@Size(min = 3, max = 15, message = "El nombre debe estar entre 7 y 15 caracteres")
	@Column(name = "nombre", length = 15, nullable = false)
	private String nombre;
	
	@NotNull(message = "La descripció es obligatoria")
	@Size(min = 3, max = 50, message = "La descripció debe estar entre 7 y 50 caracteres")
	@Column(name = "descripcion", length = 50, nullable = false, columnDefinition = "text")
	private String descripcion;
	
	public Rol() {
	}

	public Integer getIdRol() {
		return idRol;
	}

	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}