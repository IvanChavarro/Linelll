package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Pattern.Flag;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "estudiantes")
public class Estudiantes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ApiModelProperty(value = "nombre", dataType = "Karen", hidden = false, notes = "Nombre de la persona")
	@Size(min = 3, max = 18)
	@NotBlank(message = "El nombre es obligatorio")
	@Column(name = "nombre")
	private String nombre;

	@ApiModelProperty(value = "apellido", dataType = "Chavarro", notes = "Apellido de la persona")
	@Size(min = 4, max = 18)
	@NotBlank(message = "El apellido es obligatorio")
	@Column(name = "apellido")
	private String apellido;

	@ApiModelProperty(value = "telefono", dataType = "3192604502", notes = "Telefono de la persona")
	@Size(min = 8, max = 10)
	@NotNull(message = "El telefono es obligatorio")
	@Column(name = "telefono")
	private String telefono;

	@ApiModelProperty(value = "email", dataType = "ichavarro@ucundinamarca.edu.co\"", notes = "Email de la persona")
	@Size(min = 4, max = 50)
	@Email(message = "La dirección de correo no es válida", flags = { Flag.CASE_INSENSITIVE })
	@Column(name = "email")
	private String email;

	@ApiModelProperty(value = "edad", dataType = "21", notes = "Edad de la persona")
	@NotBlank(message = "La edad no puede ir vacia")
	//@Range(min = 1, max = 100, message = "La edad debe ser de 1 a 100")
	@Column(name = "edad")
	private String edad;

	@ApiModelProperty(value = "direccion", dataType = "Cra 15 # 52-8", notes = "Direccion de la persona")
	@NotBlank(message = "La direccon no puede ir vacia")
	@Column(name = "direccion")
	private String direccion;

	public Estudiantes() {

	}

	public Estudiantes(Integer id, String nombre, String apellido, String telefono, String email, String edad,
			String direccion) {
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.email = email;
		this.edad = edad;
		this.direccion = direccion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCorreo() {
		return email;
	}

	public void setCorreo(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

}
