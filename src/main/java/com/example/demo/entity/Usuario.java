package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idUsuario;
		

	@NotNull(message = "El documento es obligatorio")
	@Size(min = 7, max = 30, message = "El documento debe estar entre 7 y 12 caracteres")
	@Column(name = "documento", nullable = false, length = 30, unique = true)
	private String documento;

	@NotNull(message = "El nombre es obligatorio")
	@Size(min = 3, max = 25, message = "El nombre debe estar entre 7 y 25 caracteres")
	@Column(name = "nombre", nullable = false, length = 25)
	private String nombre;

	@NotNull(message = "El apellido es obligatorio")
	@Size(min = 7, max = 25, message = "El apellido debe estar entre 7 y 25 caracteres")
	@Column(name = "apellido", nullable = false, length = 25)
	private String apellido;
	
	@NotNull(message = "El nick es obligatorio")
	@Column(name = "nick", nullable = false, unique = true)
	private String nick;

	@NotNull(message = "La clave es obligatoria")
	//@Size(min = 7, max = 25, message = "La clave debe estar entre 7 y 25 caracteres")
	@Column(columnDefinition="TEXT", name = "clave", nullable = false, length=25)
	private String clave;

	@ManyToOne
	@JoinColumn(name = "idRol", foreignKey = @ForeignKey(name = "FK_rol"))
	private Rol rol;
	
	public Usuario() {
		
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	
}