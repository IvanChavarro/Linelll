package com.example.demo.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.entity.AutorEditorial;
import com.example.demo.exception.ArgumentRequiredException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ModelNotFoundException;

public interface ICrud<T, ID> {
	public Page<T> retornarPaginado(int page, int size);

	public Page<T> retornarPaginado(Pageable page);

	public Optional<T> retonarPorId(Integer id) throws ModelNotFoundException;

	public void guardar(T autor) throws ConflictException, ModelNotFoundException;

	public void editar(T autor) throws ArgumentRequiredException, ModelNotFoundException, ConflictException;

	public void eliminar(int id) throws ModelNotFoundException;
}
