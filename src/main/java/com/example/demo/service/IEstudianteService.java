package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.demo.dto.EstudianteDto;
import com.example.demo.entity.Estudiantes;
import com.example.demo.exception.IlegalTypeException;
import com.example.demo.exception.ModelNotFoundException;



public interface IEstudianteService {

	public List<Estudiantes> retornar();
	
	public Optional<Estudiantes> retornarUno(Integer i) throws ModelNotFoundException;

	public void guardar(Estudiantes estudiante) throws IlegalTypeException;

	public void editar(Estudiantes estudiante);

	public void eliminar(int i);
	
	//public long contarEstudiantes(Estudiantes estudiantes);
}
