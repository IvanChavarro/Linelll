package com.example.demo.service;


import com.example.demo.entity.Autor;
import com.example.demo.exception.ModelNotFoundException;

public interface IAutorService extends ICrud<Autor, Integer> {
	public Autor retornarPorIdeBoolean(Integer idAutor, boolean detaller) throws ModelNotFoundException;

	public Autor buscarDescripcion(String cedula);

}
