package com.example.demo.service;

import com.example.demo.entity.AutorEditorial;
import com.example.demo.exception.ModelNotFoundException;

public interface IAutorEditorialService extends ICrud<AutorEditorial,Integer>{

	public void eliminarNativo(Integer idAutor, Integer idEditorial) throws ModelNotFoundException;	
	
	public AutorEditorial  buscarIdNativo(Integer idAutor) throws ModelNotFoundException ;
	
	public AutorEditorial  buscarIdNativoeditorial(Integer idEditorial) throws ModelNotFoundException ;
}