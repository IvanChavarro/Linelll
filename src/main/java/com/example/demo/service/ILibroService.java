package com.example.demo.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.LibrosDto;
import com.example.demo.entity.Libro;
import com.example.demo.exception.ArgumentRequiredException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ModelNotFoundException;

@Transactional
public interface ILibroService extends ICrud<Libro, Integer>{
	public Libro guardarLibroAutor(Libro libro, int id) throws ConflictException,ModelNotFoundException,ArgumentRequiredException;
	public Libro editarLibro(Libro libro, int id) throws ModelNotFoundException, ConflictException;
	
	public List<LibrosDto>buscarDescripcion(String nombre);
	
}
