package com.example.demo.exception;

import java.time.LocalDate;

public class ExceptionWrapper {
	
	private LocalDate timestamp;

	private int status;

	private String error;

	private String message;

	private String path;

	private ExceptionWrapper() {
		
	}
	
	
	public ExceptionWrapper( int status, String error, String message, String path) {
		this.timestamp = LocalDate.now();
		this.status = status;
		this.error = error;
		this.message = message;
		this.path = path;
	}


	public LocalDate getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(LocalDate timestamp) {
		this.timestamp = timestamp;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public String getError() {
		return error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getPath() {
		return path;
	}


	public void setPath(String path) {
		this.path = path;
	}
	
	

	
	
}
