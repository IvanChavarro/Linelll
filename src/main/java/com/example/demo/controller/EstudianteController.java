package com.example.demo.controller;

import com.example.demo.dto.EstudianteDto;
import com.example.demo.entity.Estudiantes;
import com.example.demo.exception.IlegalTypeException;
import com.example.demo.exception.ModelNotFoundException;
import com.example.demo.service.IEstudianteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;



import java.util.List;
import java.util.stream.Collectors;

@RestController 
/*anotación requerida para un web service*/
@RequestMapping("/prueba") 
/*para definir el path de esta clase*/

@Validated
@Api(value = "EstudianteController", description = "controlador de estudiante")

public class EstudianteController {
	@Autowired
	@Qualifier("EstudianteServiceImpl")
	private IEstudianteService service;

	/*@ApiOperation(value = "Retorna un objeto estudianteDTO", notes = "Obtiene un usuario con un id entre 1 - 10", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "ok"), @ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 500, message = "INTERNAL SERVER ERROR"),
			@ApiResponse(code = 405, message = "METHOD NOT ALLOWED"),
			@ApiResponse(code = 400, message = "BAD REQUEST") })
	/*@ResponseStatus(HttpStatus.OK)
	@GetMapping(value = "/obtener", produces = "application/json")
	/*quiere decir que el resultado del metodo GET
	retorna un json
	el value indica el path
	la anotación @PathVariable ayuda a recibir variables
	como @PathParam*/
	/**
	 * Metodo encacrgado de retornar la lista completa de estudiantes con la documentación HATEOAS
	 * @param i
	 * @return est = lista de estudiantes
	 * @throws ModelNotFoundException se lanza en caso de no encontrar al estudiante
	 * @throws Exception
	 */
/*	public List<EntityModel<Estudiantes>> retornar()
			throws ModelNotFoundException, Exception {
		
		
		List<EntityModel<Estudiantes>> est = service.retornar().stream()
				.map(estudiante -> new EntityModel<>(estudiante,
						linkTo(methodOn(EstudianteController.class).editar(estudiante)).withRel("getThisCapability"),
						linkTo(methodOn(EstudianteController.class).eliminar(estudiante.getId()))
								.withRel("getAllCapabilities")))
				.collect(Collectors.toList());
		return est;
	}*/

	/*@ApiOperation(value = "Guarda un objeto estudianteDTO, es necesario enviar el objeto completo", notes = "Crea un nuevo usuario a partir de un ID, el usuario no debe existir", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "CREATED", response = EstudianteDto.class),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 415, message = "UNSUPPORTED MEDIA TYPE"),
			@ApiResponse(code = 500, message = "INTERNAL SERVER ERROR"),
			@ApiResponse(code = 405, message = "METHOD NOT ALLOWED"),
			@ApiResponse(code = 400, message = "BAD REQUEST") })

	@PostMapping(value = "/guadar", consumes = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public EntityModel<?> guardar(@Validated @RequestBody(required = true) Estudiantes estudiante)  throws IlegalTypeException {
		/* el @RequestBody
		 le dice
		 al front que ingrese un JSON con los datos de estudiante*/
		
		
	/*	service.guardar(estudiante);
		return EntityModel.of(estudiante,
				linkTo(methodOn(EstudianteController.class).guardar(estudiante)).withSelfRel(),
				linkTo(methodOn(EstudianteController.class).eliminar(Integer.valueOf(estudiante.getId())))
						.withSelfRel());

	}*/
/*
	@ApiOperation(value = "Edita un objeto estudianteDTO", notes = "El ID del estudiante debe existir", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "ok"), @ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 415, message = "UNSUPPORTED MEDIA TYPE"),
			@ApiResponse(code = 500, message = "INTERNAL SERVER ERROR"),
			@ApiResponse(code = 405, message = "METHOD NOT ALLOWED"),
			@ApiResponse(code = 400, message = "BAD REQUEST") })
	@PutMapping(value = "/editar", consumes = "application/json")
	@ResponseStatus(HttpStatus.OK)
	public EntityModel<?> editar(@RequestBody Estudiantes estudiante) {
		service.editar(estudiante);
		return EntityModel.of(estudiante, linkTo(methodOn(EstudianteController.class).editar(estudiante)).withSelfRel(),
				linkTo(methodOn(EstudianteController.class).eliminar(Integer.valueOf(estudiante.getId())))
						.withSelfRel());
	}

	
	// El eliminar retorna codigo 204 SIEMPRE
	@ApiOperation(value = "Elimina un objeto estudianteDTO", notes = "El ID del estudiante debe existir", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 204, message = "NO CONTENT"),
			@ApiResponse(code = 404, message = "NOT FOUND"),
			@ApiResponse(code = 415, message = "UNSUPPORTED MEDIA TYPE"),
			@ApiResponse(code = 500, message = "INTERNAL SERVER ERROR"),
			@ApiResponse(code = 405, message = "METHOD NOT ALLOWED"),
			@ApiResponse(code = 400, message = "BAD_REQUEST") })
	@DeleteMapping(value = "/eliminar/{x}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public EntityModel<?> eliminar(@PathVariable int x) {
		service.eliminar(x);
		return EntityModel.of(linkTo(methodOn(EstudianteController.class).eliminar(x)).withSelfRel(),
				linkTo(methodOn(EstudianteController.class).eliminar(x)).withSelfRel());
	}*/
}
