package com.example.demo.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Editorial;
import com.example.demo.exception.ArgumentRequiredException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ModelNotFoundException;
import com.example.demo.service.IEditorialService;



@RestController
@RequestMapping("/editorial")
public class EditorialController {
	@Autowired
	private IEditorialService service;
	
	@GetMapping(value={"/obtenerPaginado/{page}/{size}"}, produces = "application/json")
	public ResponseEntity<?> retornarPaginado(@PathVariable  int page, @PathVariable int size){
		Page<Editorial>listaEditorial= service.retornarPaginado(page, size);
		return new ResponseEntity<Page<Editorial>>(listaEditorial,HttpStatus.OK);
	}
	
	@GetMapping(value = "/obtenerPaginado" ,produces = "application/json")
	public ResponseEntity<?> retonarPaginado(Pageable page) {
		Page<Editorial> listaEditorial = service.retornarPaginado(page);
		return new ResponseEntity<Page<Editorial>>(listaEditorial, HttpStatus.OK);	
	}	
	
	@PostMapping(value = "/insertar", consumes = "application/json")
	public ResponseEntity<?> guardar(@Valid @RequestBody Editorial editorial) throws ConflictException, ModelNotFoundException {
		service.guardar(editorial);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@PutMapping(value = "/editar", consumes = "application/json")
	public ResponseEntity<?> editar(@Valid @RequestBody Editorial editorial)throws ArgumentRequiredException, ModelNotFoundException, ConflictException{	
		this.service.editar(editorial);
		return new ResponseEntity<Editorial>(HttpStatus.OK);
	}
	
	@GetMapping(value={"/obtener/{i}"}, produces = "application/json")
	public ResponseEntity<?> retornar(@PathVariable  Integer i) throws Exception{
		Optional<Editorial> editorial = service.retonarPorId(i);
		return new ResponseEntity<Optional<Editorial>>(editorial,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/eliminar/{i}")
	public ResponseEntity<?> eliminar(@PathVariable int i) throws ModelNotFoundException {
		service.eliminar(i);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
}
