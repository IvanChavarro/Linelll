package com.example.demo.controller;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.LibrosDto;
import com.example.demo.entity.Libro;
import com.example.demo.exception.ArgumentRequiredException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ModelNotFoundException;
import com.example.demo.service.ILibroService;

@RestController
@RequestMapping("/libros")
public class LibroController {
	@Autowired
	private ILibroService service;

	@GetMapping(value = "/obtenerPaginado", produces = "application/json")
	public ResponseEntity<?> retornarPaginado(Pageable page) {
		Page<Libro> libros = service.retornarPaginado(page);
		return new ResponseEntity<Page<Libro>>(libros, HttpStatus.OK);
	}
	
	@GetMapping(value = "/obtenerDescripcion/{nombre}", produces = "application/json")
	public ResponseEntity<?> retornarDescripcion(@PathVariable String nombre) {
		List<LibrosDto> libros = service.buscarDescripcion(nombre);
		return new ResponseEntity<List<LibrosDto>>(libros, HttpStatus.OK);
	}


	@GetMapping(value = "/obtenerPorId/{id}", produces = "application/json")
	public ResponseEntity<?> retornarPorId(@PathVariable Integer id) throws ModelNotFoundException {
		System.out.println(id);
		Optional<Libro> autor = service.retonarPorId(id);
		return new ResponseEntity<Optional<Libro>>(autor, HttpStatus.OK);
	}

	@PostMapping(value = "/guardar/{id}", consumes = "application/json")
	public ResponseEntity<?> guardar(@Valid @RequestBody(required = true) Libro libro, @PathVariable Integer id)
			throws ConflictException, ModelNotFoundException, ArgumentRequiredException {
		service.guardarLibroAutor(libro, id);
		return new ResponseEntity<Libro>(HttpStatus.CREATED);
	}

	@PutMapping(value = "/editar/{id}", consumes = "application/json")
	public ResponseEntity<?> editar(@Valid @RequestBody(required = true) Libro libro, @PathVariable Integer id) throws ModelNotFoundException, ConflictException {
		service.editarLibro(libro, id);
		return new ResponseEntity<Libro>(HttpStatus.OK);
	}

	@DeleteMapping(value = "/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable int id) throws ModelNotFoundException {
		service.eliminar(id);
		return new ResponseEntity<Libro>(HttpStatus.NO_CONTENT);
	}
}
