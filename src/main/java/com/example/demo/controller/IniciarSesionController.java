package com.example.demo.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.entity.Usuario;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ModelNotFoundException;
import com.example.demo.service.IUsuarioService;



@RestController
@RequestMapping("/iniciarsesion")
public class IniciarSesionController {

	@Autowired
	private IUsuarioService service;
	
	@GetMapping(value={"/obtenerPaginado/{page}/{size}"}, produces = "application/json")
	public ResponseEntity<?> retornarPaginado(@PathVariable  int page, @PathVariable int size){
		Page<Usuario>listausuario= service.retornarPaginado(page, size);
		return new ResponseEntity<Page<Usuario>>(listausuario,HttpStatus.OK);
	}
	
	
	@PostMapping(value = "/insertar", consumes = "application/json")
	public ResponseEntity<?> guardar(@Valid @RequestBody Usuario usuario) throws ConflictException, ModelNotFoundException {
		service.guardar(usuario);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	
	@DeleteMapping(value = "/eliminar/{i}")
	public ResponseEntity<?> eliminar(@PathVariable int i) throws ModelNotFoundException {
		service.eliminar(i);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}