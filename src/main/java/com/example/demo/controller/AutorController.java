package com.example.demo.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.entity.Autor;
import com.example.demo.entity.AutorEditorial;
import com.example.demo.exception.ArgumentRequiredException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ModelNotFoundException;
import com.example.demo.service.IAutorEditorialService;
import com.example.demo.service.IAutorService;

@PreAuthorize("hasAuthority('Estudiante')")
@RestController
@RequestMapping("/autores")
public class AutorController {

	@Autowired
	private IAutorService iAutorService;
	@Autowired
	private IAutorEditorialService serviceAE;
	
	
	@GetMapping(value = "/obtenerPaginado", produces = "application/json")
	public ResponseEntity<?> retornarPaginado(Pageable page) {
		Page<Autor> listaAutor = iAutorService.retornarPaginado(page);
		return new ResponseEntity<Page<Autor>>(listaAutor, HttpStatus.OK);
	}
	
	@GetMapping(value = "/obtenerPorCedula/{cedula}", produces = "application/json")
	public ResponseEntity<?> retornarPorCedula(@PathVariable String cedula) {
		Autor listaAutor = iAutorService.buscarDescripcion(cedula);
		return new ResponseEntity<Autor>(listaAutor, HttpStatus.OK);
	}
	
	@GetMapping(value = "/obtenerPorId/{id}/{detalle}", produces = "application/json")
	public ResponseEntity<?> retornarPorId(@PathVariable Integer id, @PathVariable boolean detalle) throws ModelNotFoundException {
		System.out.println(id);
		Autor autor = iAutorService.retornarPorIdeBoolean(id,detalle);
		return new ResponseEntity <Autor>(autor, HttpStatus.OK);
	}
	
	@PostMapping(value = "/guardar", consumes = "application/json")
	public ResponseEntity<?> guardar(@Valid @RequestBody Autor autor) throws ConflictException, ModelNotFoundException{
		iAutorService.guardar(autor);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@DeleteMapping(value = "/eliminar/{id}")
	public ResponseEntity<?> eliminar(@PathVariable int id) throws ModelNotFoundException{
		iAutorService.eliminar(id);
		return new ResponseEntity<Autor>(HttpStatus.NO_CONTENT);
	}
	
	@PutMapping(value="/editar", consumes = "application/json")
	public ResponseEntity<?> editar(@RequestBody(required = true) Autor autor) throws ArgumentRequiredException, ModelNotFoundException, ConflictException{
		iAutorService.editar(autor);
		return new ResponseEntity <Autor>(HttpStatus.OK);
	}
	
	@PostMapping(value = "/asociarEditorial", consumes = "application/json")
	public ResponseEntity<?> guardar(@Valid @RequestBody AutorEditorial autorEditorial) throws ConflictException, ModelNotFoundException {
		serviceAE.guardar(autorEditorial);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/obtenerPaginado/asociadoEditorial" ,produces = "application/json")
	public ResponseEntity<?> retonarPaginados(Pageable page) {
		Page<AutorEditorial>listaAE = serviceAE.retornarPaginado(page);
		return new ResponseEntity<Page<AutorEditorial>>(listaAE, HttpStatus.OK);	
	}	
	
	@GetMapping(value={"/obtenerPaginado/asociadoEditorial/{page}/{size}"}, produces = "application/json")
	public ResponseEntity<?> retornarPaginados(@PathVariable  int page, @PathVariable int size){
		Page<AutorEditorial>listaAutorE= serviceAE.retornarPaginado(page, size);
		return new ResponseEntity<Page<AutorEditorial>>(listaAutorE,HttpStatus.OK);
	}
	
	@GetMapping(value={"/asociadoEditorial/obtener/{i}"}, produces = "application/json")
	public ResponseEntity<?> retornarid(@PathVariable  Integer i) throws ModelNotFoundException{
		AutorEditorial autore =  serviceAE.buscarIdNativo(i);
		return new ResponseEntity<AutorEditorial>(autore,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/asociadoEditorial/eliminar/{i}/{j}")
	public ResponseEntity<?> eliminarae(@PathVariable int i, @PathVariable int j) throws ModelNotFoundException {
		serviceAE.eliminarNativo(i, j);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
