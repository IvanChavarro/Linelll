package com.example.demo.serviceImplement;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Editorial;
import com.example.demo.exception.ArgumentRequiredException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ModelNotFoundException;
import com.example.demo.repository.IEditorialRepo;
import com.example.demo.service.IEditorialService;

@Service
public class EditorialServiceImplement implements IEditorialService{

@Autowired
private IEditorialRepo repo;

@Override
public Page<Editorial> retornarPaginado(int page, int size) {
	return repo.findAll(PageRequest.of(page, size));
}

@Override
public Page<Editorial> retornarPaginado(Pageable page) {
	Page<Editorial>pageeditorial =repo.findAll(page);
	return repo.findAll(page);
}

@Override
public Optional<Editorial> retonarPorId(Integer idEstudiante) throws ModelNotFoundException {
	return Optional.of(repo.findById(idEstudiante).orElseThrow(() -> new ModelNotFoundException("Editorial no encontrado")));
}

@Override
public void guardar(Editorial estudiante) throws ConflictException {
	this.repo.save(estudiante);
}

@Override
public void editar(Editorial editorial) throws ArgumentRequiredException, ModelNotFoundException, ConflictException {
	
	if (editorial.getId() != null) {
		if(validarExistenciaPorId(editorial.getId())) {
			this.repo.save(editorial);
		}else {
			throw new ModelNotFoundException("Editorial no encontrado");
		}
	} else {
		throw new ArgumentRequiredException("Id de la editorial Requerido");
	}
	
}

@Override
public void eliminar(int idEstudiante) throws ModelNotFoundException {
	if(validarExistenciaPorId(idEstudiante)) {
		this.repo.deleteById(idEstudiante);
	}else {
		throw new ModelNotFoundException("Editorial no encontrado");
	}
	
}

private Boolean validarExistenciaPorId(int i) {
	return repo.existsById(i);
}

}
