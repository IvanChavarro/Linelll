package com.example.demo.serviceImplement;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.example.demo.entity.Rol;
import com.example.demo.exception.ArgumentRequiredException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ModelNotFoundException;
import com.example.demo.repository.IRolRepo;
import com.example.demo.service.IRolService;

@Service
public class RolServiceImplement implements IRolService {

	@Autowired
	private IRolRepo repo;
	
	@Override
	public Page<Rol> retornarPaginado(int page, int size) {
		return repo.findAll(PageRequest.of(page, size));
	}

	@Override
	public Page<Rol> retornarPaginado(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Optional<Rol> retonarPorId(Integer idEstudiante) throws ModelNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardar(Rol rol) throws ConflictException {
		this.repo.save(rol);
		
	}

	@Override
	public void editar(Rol estudiante) throws ArgumentRequiredException, ModelNotFoundException, ConflictException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminar(int idRol) throws ModelNotFoundException {
		this.repo.deleteById(idRol);
		
	}

}
