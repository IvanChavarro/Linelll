package com.example.demo.serviceImplement;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.dto.LibrosDto;
import com.example.demo.entity.Autor;
import com.example.demo.entity.Libro;
import com.example.demo.exception.ArgumentRequiredException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ModelNotFoundException;
import com.example.demo.repository.IAutorRepo;
import com.example.demo.repository.ILibroRepo;
import com.example.demo.service.ILibroService;

@Service
public class LibroServiceImplement implements ILibroService {

	@Autowired
	private ILibroRepo iLibroRepo;

	@Autowired
	private IAutorRepo iAutorRepo;

	@Override
	public Page<Libro> retornarPaginado(int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Libro> retornarPaginado(Pageable page) {
		// TODO Auto-generated method stub
		return this.iLibroRepo.findAll(page);
	}

	@Override
	public Optional<Libro> retonarPorId(Integer idAutor) throws ModelNotFoundException {
		// TODO Auto-generated method stub
		if (!this.iLibroRepo.existsById(idAutor))
			throw new ModelNotFoundException("El id no existe");
		Optional<Libro> libro = this.iLibroRepo.findById(idAutor);
		return libro;
	}

	@Override
	public void guardar(Libro autor) throws ConflictException {

	}

	@Override
	public void editar(Libro autor) throws ArgumentRequiredException, ModelNotFoundException, ConflictException {
		// TODO Auto-generated method stub
	}

	@Override
	public void eliminar(int idAutor) throws ModelNotFoundException {
		if (!this.iLibroRepo.existsById(idAutor))
			throw new ModelNotFoundException("El id no existe");

		this.iLibroRepo.deleteById(idAutor);

	}

	@Override
	public Libro guardarLibroAutor(Libro libro, int id)
			throws ConflictException, ModelNotFoundException, ArgumentRequiredException {
		Autor autor = iAutorRepo.findById(id).orElseThrow(() -> new ModelNotFoundException("Autor no encontrado"));
		if (iLibroRepo.existsByNombreAndAutor(libro.getNombre(), autor))
			throw new ConflictException("Ya existe un libro con el nombre " + libro.getNombre());
		libro.setAutor(autor);
		return this.iLibroRepo.save(libro);
	}

	@Override
	public Libro editarLibro(Libro libro, int id) throws ModelNotFoundException, ConflictException {
		Autor autor = iAutorRepo.findById(id).orElseThrow(() -> new ModelNotFoundException("Autor no encontrado"));
		libro.setAutor(autor);
		return this.iLibroRepo.save(libro);
	}

	@Override
	public List<LibrosDto> buscarDescripcion(String nombre) {
		List<Libro> listaLibros = iLibroRepo.buscarDescripcion(nombre);
		List<LibrosDto> listaLibrosDto = new ArrayList<>();
		for (Libro lista : listaLibros) {
			ModelMapper modelMapper = new ModelMapper();
			LibrosDto acudienteDto = modelMapper.map(lista, LibrosDto.class);
			listaLibrosDto.add(acudienteDto);
		}
		return listaLibrosDto;
	}

}
