package com.example.demo.serviceImplement;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.example.demo.entity.Estudiantes;
import com.example.demo.service.IEstudianteService;
import com.example.demo.exception.ModelNotFoundException;
import com.example.demo.repository.IEstudianteRepo;

/**
 * EstudianteServiceImpl
 */
@Component("EstudianteServiceImpl")
@Service
public class EstudianteServiceImplement implements IEstudianteService {

	@Autowired
	IEstudianteRepo ier;
	
	@Override
	public List<Estudiantes> retornar(){
			List<Estudiantes> est = ier.findAll(); 
		return est;

	}
	
	@Override
	public Optional<Estudiantes> retornarUno(Integer i) throws ModelNotFoundException {
		Optional<Estudiantes> est = ier.findById(i);
		return est;
	}

	@Override
	public void guardar(Estudiantes estudiante) {
		// TODO Auto-generated method stub
		ier.save(estudiante);
	}

	@Override
	public void editar(Estudiantes estudiante) {
		ier.save(estudiante);
	}

	@Override
	public void eliminar(int i) {
		// TODO Auto-generated method stub
		ier.deleteById(i);
	}

	/*@Override
	
	public long contarEstudiantes(Estudiantes estudiantes) {
		// TODO Auto-generated method stub
		Boolean b = ier.count(estudiantes	); 
		
		return cant;
	}*/

	

}