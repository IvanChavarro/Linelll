package com.example.demo.serviceImplement;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.example.demo.MHelpers;
import com.example.demo.dto.AutoresDto;
import com.example.demo.dto.LibrosDto;
import com.example.demo.entity.Autor;
import com.example.demo.entity.Libro;
import com.example.demo.exception.ArgumentRequiredException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ModelNotFoundException;
import com.example.demo.repository.IAutorRepo;
import com.example.demo.service.IAutorService;

@Service
public class AutorServiceImplement implements IAutorService {
	@Autowired
	private IAutorRepo repo;

	public static final ModelMapper modelMapper = new ModelMapper();

	@Override
	public Page<Autor> retornarPaginado(int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Autor> retornarPaginado(Pageable page) {
		 
		Page<Autor> pageable = repo.findAll(page);
		pageable.getContent().forEach(autor ->{
			autor.setLibro(null);
		});
		return pageable;
	}

	@Override
	public Optional<Autor> retonarPorId(Integer idAutor) throws ModelNotFoundException {
		if (!this.repo.existsById(idAutor))
			throw new ModelNotFoundException("El id no existe");

		Optional<Autor> autor = this.repo.findById(idAutor);
		return autor;
	}

	@Override
	public void guardar(Autor autor) throws ConflictException {
		if (repo.existsByCorreo(autor.getCorreo())) {
			throw new ConflictException("El correo ya existe");
		}
		if (repo.existsByCedula(autor.getCedula())) {
			throw new ConflictException("La cedula ya existe");
		}
		if (autor.getLibro() != null) {
			autor.getLibro().forEach(det -> {
				det.setAutor(autor);
			});
		}
		this.repo.save(autor);
	}

	@Override
	public void editar(Autor autor) throws ConflictException, ModelNotFoundException {

		Autor autorError = repo.findById(autor.getId())
				.orElseThrow(() -> new ModelNotFoundException("El autor no existe"));
		String correo = autorError.getCorreo();
		if (autor.getCorreo().equals(correo)) {
			if (autor.getLibro() != null) {
				autor.getLibro().forEach(det -> {
					det.setAutor(autor);
				});
			}
			this.repo.save(autor);
		} else if (!repo.existsByCorreo(autor.getCorreo())) {
			if (autor.getLibro() != null) {
				autor.getLibro().forEach(det -> {
					det.setAutor(autor);
				});
			}
			this.repo.save(autor);
		} else {
			throw new ConflictException("El correo ya existe");
		}

		if (autor.getId() == null)
			throw new ConflictException("El id es requerido");
	}

	@Override
	public void eliminar(int idAutor) throws ModelNotFoundException {
		if (!this.repo.existsById(idAutor))
			throw new ModelNotFoundException("El id no existe");
		this.repo.deleteById(idAutor);

	}

	@Override
	public Autor retornarPorIdeBoolean(Integer idAutor, boolean detaller) throws ModelNotFoundException {
		Autor autor = repo.findById(idAutor).orElseThrow(() -> new ModelNotFoundException("Autor no encontrado"));
		if (!detaller) {
			autor.setLibro(new ArrayList<>());
		}
		return autor;
	}

	@Override
	public Autor buscarDescripcion(String cedula) {
		Autor autor =repo.buscarPorCedula(cedula) ;
		return autor;
	}
}
