package com.example.demo.serviceImplement;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Autor;
import com.example.demo.entity.AutorEditorial;
import com.example.demo.entity.Editorial;
import com.example.demo.exception.ArgumentRequiredException;
import com.example.demo.exception.ConflictException;
import com.example.demo.exception.ModelNotFoundException;
import com.example.demo.repository.IAutorEditorialRepo;
import com.example.demo.repository.IAutorRepo;
import com.example.demo.repository.IEditorialRepo;
import com.example.demo.service.IAutorEditorialService;

@Service
public class AutorEditorialServiceImplement implements IAutorEditorialService{

	@Autowired
	private IAutorEditorialRepo repo;
	
	@Autowired
	private IAutorRepo repoAutor;
	
	@Autowired
	private IEditorialRepo repoEditorial;
	
	@Override
	public Page<AutorEditorial> retornarPaginado(int page, int size) {
		return repo.findAll(PageRequest.of(page, size));
	}

	@Override
	public Page<AutorEditorial> retornarPaginado(Pageable page) {
		return repo.findAll(page);
	}

	@Override
	public Optional<AutorEditorial> retonarPorId(Integer idEstudiante) throws ModelNotFoundException {
		return null;
	}
	
	public Optional<Autor> retornarAutor(Integer idAutor)throws ModelNotFoundException{
		return Optional.ofNullable(this.repoAutor.findById(idAutor).orElseThrow(()->new ModelNotFoundException("El autor no existe")));
	}
	
	public Optional<Editorial> retornarEditorial(Integer idEditorial)throws ModelNotFoundException{
		return Optional.ofNullable(this.repoEditorial.findById(idEditorial).orElseThrow(()->new ModelNotFoundException("la Editorial no existe")));
	}

	@Override
	public void guardar(AutorEditorial estudiante) throws ConflictException, ModelNotFoundException {
		if(retornarAutor(estudiante.getAutor().getId())==null) {
			throw new ModelNotFoundException("Autor no encontrado");
		}else {
			if(retornarEditorial(estudiante.getEditorial().getId())==null) {
				throw new ModelNotFoundException("Editorial no encontrado");
			}else {
				this.repo.guardarNativo(estudiante.getAutor().getId(), estudiante.getEditorial().getId(),estudiante.getFecha());	
			}
		}	
	}

	@Override
	public void editar(AutorEditorial estudiante)
			throws ArgumentRequiredException, ModelNotFoundException, ConflictException {
		// TODO Auto-generated method stub
		//no se hace
		
	}

	@Override
	public void eliminar(int idEstudiante) throws ModelNotFoundException {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void eliminarNativo(Integer idAutor, Integer idEditorial) throws ModelNotFoundException {	
		if(buscarIdNativo(idAutor)==null) {
			throw new ModelNotFoundException("Autor no encontrado");
		}else {
			if(buscarIdNativoeditorial(idEditorial)==null) {
				throw new ModelNotFoundException("Editorial no encontrado");
			}else {
				this.repo.eliminarNativa(idAutor, idEditorial);	
			}
		}
	}

	@Override
	public AutorEditorial  buscarIdNativo(Integer idAutor) throws ModelNotFoundException {
		AutorEditorial autore = repo.buscarIdNativo(idAutor);
		if(autore==null) {
			throw new ModelNotFoundException("Autor no encontrado");
		}else {
			return autore;
		}
	}

	@Override
	public AutorEditorial buscarIdNativoeditorial(Integer idEditorial) throws ModelNotFoundException {
		AutorEditorial autore = repo.buscarIdNativoeditorial(idEditorial);
		if(autore==null) {
			throw new ModelNotFoundException("Editorial no encontrado");
		}else {
			return autore;
		}
	}

	

}