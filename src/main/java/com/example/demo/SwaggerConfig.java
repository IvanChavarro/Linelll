package com.example.demo;

import org.springframework.context.annotation.Bean;

import org.springframework.plugin.core.SimplePluginRegistry;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//import org.springframework.hateoas.client.LinkDiscoverer
//import org.springframework.hateoas.client.LinkDiscoverers;
//import org.springframework.hateoas.mediatype.collectionjson.CollectionJsonLinkDiscoverer;
import org.springframework.plugin.core.SimplePluginRegistry;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).useDefaultResponseMessages(false).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("cundi.edu.co.demo.controller")).paths(PathSelectors.any())
				.build();
	}

	// Clase para agregar información a la documentación
	private ApiInfo apiInfo() {
		// TODO Auto-generated method stub
		return new ApiInfoBuilder().title("Documentación Spring ")
				.description("Interactua con los metodos get, post, put y delete").termsOfServiceUrl("Open Source")
				.license("©Antiproblema License").version("2.0").build();
	}

}
