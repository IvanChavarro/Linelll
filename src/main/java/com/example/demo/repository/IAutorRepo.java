package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.Autor;

@Repository
public interface IAutorRepo extends JpaRepository<Autor, Integer> {
	public boolean existsByCorreo(String correo);
	public boolean existsByCedula(String cedula);
	public boolean existsById(Integer id);
	
	@Query(value="Select * FROM autor  where cedula = :cedula", nativeQuery = true)
	public Autor buscarPorCedula(String cedula);
}
