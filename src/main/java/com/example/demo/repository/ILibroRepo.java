package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.LibrosDto;
import com.example.demo.entity.Autor;
import com.example.demo.entity.Libro;

@Transactional
public interface ILibroRepo extends JpaRepository<Libro, Integer> {
	public boolean existsByNombre(String nombre);
	public boolean existsById(Integer id);
	public Boolean existsByNombreAndAutor(String nombre, Autor autor);
	public Libro findByNombre(String nombre);

	@Query("SELECT n FROM Libro n where n.nombre = :nombre")
	public List<Libro>buscarDescripcion(String nombre);
}
