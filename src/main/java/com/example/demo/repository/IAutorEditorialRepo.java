package com.example.demo.repository;

import java.time.LocalDate;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.AutorEditorial;


@Repository
public interface IAutorEditorialRepo extends JpaRepository<AutorEditorial,Integer> {

	@Transactional
	@Modifying
	@Query(value = "INSERT INTO autor_editorial(id_autor, id_editorial, fecha) VALUES(:idAutor, :idEditorial, :fecha)"
				,nativeQuery = true)
	void guardarNativo(@Param("idAutor") Integer idAutor, @Param("idEditorial") Integer idEditorial, @Param("fecha") LocalDate fecha);
	
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM autor_editorial where id_autor = :idAutor and id_editorial = :idEditorial ",  nativeQuery = true)
	 void eliminarNativa(@Param("idAutor") Integer idAutor, @Param("idEditorial") Integer idEditorial);
	
	 @Query(nativeQuery=true,value = "select * from autor_editorial where  id_autor = :idAutor")
	 public AutorEditorial buscarIdNativo(@Param("idAutor") Integer idAutor);
	 
	 @Query(nativeQuery=true,value = "select * from id_editorial where  id_autor = :idEditorial")
	 public AutorEditorial buscarIdNativoeditorial(@Param("idEditorial") Integer idEditorial);
}