package com.example.demo.dto;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern.Flag;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Entidad estudiante")//Para cambiar el nombre del modelo en la documentacióm
@Validated
public class EstudianteDto {
	
	@ApiModelProperty(value="nombre", dataType = "Karen", hidden = false,notes="Nombre de la persona")
	@Size(min = 3, max = 18)
	@NotBlank(message = "El nombre es obligatorio")
	private String nombre;
	
	@ApiModelProperty(value="apellido", dataType = "Chavarro",notes="Apellido de la persona")
	@Size(min = 4, max = 18)
	@NotBlank(message = "El apellido es obligatorio")
	private String apellido;

	@ApiModelProperty(value="telefono", dataType = "3192604502",notes="Telefono de la persona")
	@Size(min = 8, max = 10)
	@NotNull(message = "El telefono es obligatorio")
	private String telefono;
	
	@ApiModelProperty(value="email", dataType = "ichavarro@ucundinamarca.edu.co\"",notes="Email de la persona")
	@Size(min = 4, max = 50)
	@Email(message = "La dirección de correo no es válida", flags = { Flag.CASE_INSENSITIVE })
	private String email;

	@ApiModelProperty(value="edad", dataType = "21",notes="Edad de la persona")
	@Size(min = 1, max = 2)
	@NotBlank(message = "La edad no puede ir vacia")
	@Range(min = 1, max = 100, message = "La edad debe ser de 1 a 100")
	private String edad;
	
	@ApiModelProperty(value="direccion", dataType = "Cra 15 # 52-8",notes="Direccion de la persona")
	@NotBlank(message = "La direccon no puede ir vacia")
	private String direccion;

	public EstudianteDto() {

	}

	public EstudianteDto(String nombre, String apellido, String telefono, String email, String edad, String direccion,
			LocalDate fecha) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.email = email;
		this.edad = edad;
		this.direccion = direccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

}
